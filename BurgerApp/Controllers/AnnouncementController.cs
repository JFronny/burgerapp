﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnnouncementController : ControllerBase
    {
        [HttpGet(Name = "GetAnnouncements")]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, Message>))]
        [ProducesResponseType(401)]
        public ActionResult<IDictionary<string, Message>> GetAnnouncements(Guid session)
        {
            if (Sessions.IsValid(session, Privileges.ListAnnouncements))
                return Ok(Announcements.GetSendable()
                    .OrderByDescending(s => s.Value.Timestamp)
                    .ToDictionary(s => s.Key, s => s.Value));
            else
                return Unauthorized();
        }

        [HttpPut(Name = "CreateAnnouncement")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PutAnnouncements(Guid session, string message)
        {
            if (Sessions.IsValid(session, Privileges.AddAnnouncement))
            {
                Guid id;
                do
                    id = Guid.NewGuid();
                while (Announcements.ContainsKey(id));
                Announcements.Add(id, new Message {Author = Sessions[session].Citizen, Content = message, Timestamp = DateTime.Now});
                return id;
            }
            return Unauthorized();
        }

        [HttpDelete(Name = "DeleteAnnouncement")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult DeleteAnnouncements(Guid session, Guid announcement)
        {
            if (Sessions.IsValid(session, Privileges.DeleteAnnouncement))
            {
                if (Announcements.Remove(announcement))
                    return Ok();
                return NotFound();
            }
            return Unauthorized();
        }
    }
}