﻿using System;
using System.Collections.Generic;
using BurgerApp.Database;
using Microsoft.AspNetCore.Mvc;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CitizenController : ControllerBase
    {
        [HttpGet(Name = "GetCitizens")]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, Citizen>))]
        [ProducesResponseType(401)]
        public ActionResult<IDictionary<string, Citizen>> GetCitizens(Guid session)
        {
            if (Sessions.IsValid(session, Privileges.ListCitizens))
                return Ok(Citizens.GetSendable());
            else if (Sessions.IsValid(session))
                return Ok(
                    new Dictionary<string, Citizen>(new[] {new KeyValuePair<string, Citizen>(Sessions[session].Citizen.ToString(), Citizens[Sessions[session].Citizen])}));
            else
                return Unauthorized();
        }

        [HttpPut(Name = "CreateCitizen")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PutCitizens(Guid session, Citizen citizen)
        {
            if (Sessions.IsValid(session, Privileges.AddCitizen))
            {
                Guid id;
                do
                    id = Guid.NewGuid();
                while (Citizens.ContainsKey(id));
                Citizens.Add(id, citizen);
                return id;
            }
            return Unauthorized();
        }

        [HttpDelete(Name = "DeleteCitizen")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult DeleteCitizens(Guid session, Guid citizen)
        {
            if (Sessions.IsValid(session, Privileges.RemoveCitizen))
            {
                if (Citizens.Remove(citizen))
                    return Ok();
                return NotFound();
            }
            return Unauthorized();
        }
        
        [HttpPatch("pwd", Name = "ChangeCitizenPassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult PatchCitizens(Guid session, Guid citizen, string password)
        {
            if (Sessions.IsValid(session, Privileges.ModifyPassword, citizen)
                || Sessions.IsValid(session, Privileges.ModifyPasswordArbitrary))
            {
                if (Citizens.ContainsKey(citizen))
                {
                    Citizen c = Citizens[citizen];
                    c.PasswordHash = Hash(password);
                    Citizens[citizen] = c;
                    return Ok();
                }
                else
                    return NotFound();
            }
            else
                return Unauthorized();
        }
        
        [HttpPatch("name", Name = "ChangeCitizenName")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult PatchCitizens(Guid session, Guid citizen, Name name)
        {
            if (Sessions.IsValid(session, Privileges.ModifyNamesArbitrary))
            {
                if (Citizens.ContainsKey(citizen))
                {
                    Citizen c = Citizens[citizen];
                    c.Name = name;
                    Citizens[citizen] = c;
                    return Ok();
                }
                else
                    return NotFound();
            }
            else
                return Unauthorized();
        }
        
        [HttpPatch("priv", Name = "ChangeCitizenPrivileges")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult PatchCitizens(Guid session, Guid citizen, Privileges privileges)
        {
            if (Sessions.IsValid(session, Privileges.ModifyPrivilegesArbitrary))
            {
                if (Citizens.ContainsKey(citizen))
                {
                    Citizen c = Citizens[citizen];
                    c.Privileges = privileges;
                    Citizens[citizen] = c;
                    return Ok();
                }
                else
                    return NotFound();
            }
            else
                return Unauthorized();
        }
    }
}