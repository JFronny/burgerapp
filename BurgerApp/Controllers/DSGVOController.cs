using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using CC_Functions.AspNet;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("[controller].zip")]
    public class DsgvoController : ControllerBase
    {
        private JsonSerializerOptions _options = new JsonSerializerOptions().AddCcf();
        [HttpGet(Name = "GetZip")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        public ActionResult GetZip(Guid session)
        {
            if (Sessions.IsValid(session))
            {
                Guid sId = Sessions[session].Citizen;
                Citizen citizen = Citizens[sId];
                using MemoryStream ms = new MemoryStream();
                using (ZipArchive archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    ICollection<Message> announcements = Announcements.Values;
                    ICollection<Poll> polls = Polls.Values;
                    ICollection<Request> requests = Requests.Values;
                    if (announcements.Any(s => s.Author == sId))
                        CreateJsonEntry(archive, "announcements.json", announcements.Where(s => s.Author == sId));
                    CreateJsonEntry(archive, "citizen.json", citizen);
                    if (polls.Any(s => s.Author == sId || s.Votes.Any(s => s.Key == sId)))
                        archive.CreateEntry("polls/");
                    if (polls.Any(s => s.Author == sId))
                        CreateJsonEntry(archive, "polls/created.json", polls.Where(s => s.Author == sId));
                    if (polls.Any(s => s.Votes.Any(t => t.Key == sId)))
                        CreateJsonEntry(archive, "polls/votes.json", polls.Select(s =>
                            new KeyValuePair<string, string>(s.Title,
                                s.Options[s.Votes.First(t => t.Key == sId).Value])));
                    if (requests.Any(s => s.MainMessage.Author == sId || s.Answers.Any(t => t.Author == sId)))
                        archive.CreateEntry("requests/");
                    if (requests.Any(s => s.MainMessage.Author == sId))
                        CreateJsonEntry(archive, "requests/created.json", requests
                            .Where(s => s.MainMessage.Author == sId)
                            .Select(s => new AnonymizedRequest
                            {
                                Title = s.Title,
                                Message = new AnonymizedMessage
                                {
                                    Content = s.MainMessage.Content,
                                    Timestamp = s.MainMessage.Timestamp
                                }
                            }));
                    if (requests.Any(s => s.Answers.Any(t => t.Author == sId)))
                        CreateJsonEntry(archive, "requests/messages.json", requests
                            .Where(s => s.Answers.Any(t => t.Author == sId))
                            .ToDictionary(s => s.Title, s => s.Answers
                                .Where(t => t.Author == sId)
                                .Select(t => new AnonymizedMessage
                                {
                                    Content = t.Content,
                                    Timestamp = t.Timestamp
                                })));
                }
                return new FileContentResult(ms.ToArray(), "application/zip");
            }
            else
                return Unauthorized();
        }

        private ZipArchiveEntry CreateJsonEntry<T>(ZipArchive archive, string name, T content)
        {
            using MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(content, _options)));
            ZipArchiveEntry entry = archive.CreateEntry(name, CompressionLevel.Optimal);
            using Stream es = entry.Open();
            ms.CopyTo(es);
            return entry;
        }
    }
}