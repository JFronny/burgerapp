using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PollController : ControllerBase
    {
        [HttpGet(Name = "GetPolls")]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, AnonymizedPoll>))]
        [ProducesResponseType(401)]
        public ActionResult<IDictionary<string, Poll>> GetPolls(Guid session)
        {
            if (Sessions.IsValid(session, Privileges.ListPolls))
                return Ok(Polls.GetAnon());
            return Unauthorized();
        }

        [HttpGet("vote", Name = "GetVote")]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult<int> GetPolls(Guid session, Guid poll)
        {
            if (Sessions.IsValid(session, Privileges.VoteOnPolls))
            {
                if (!Polls.ContainsKey(poll))
                    return NotFound();
                Poll p = Polls[poll];
                Guid citizen = Sessions[session].Citizen;
                if (p.Votes.ContainsKey(citizen))
                    return Ok(p.Votes[citizen]);
                else
                    return NoContent();
            }
            return Unauthorized();
        }

        [HttpPost("vote", Name = "Vote")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult PostPolls(Guid session, Guid poll, int vote)
        {
            if (Sessions.IsValid(session, Privileges.VoteOnPolls))
            {
                if (!Polls.ContainsKey(poll))
                    return NotFound();
                Poll p = Polls[poll];
                if (vote >= p.Options.Length)
                    return BadRequest();
                Guid citizen = Sessions[session].Citizen;
                if (p.Votes.ContainsKey(citizen))
                    p.Votes[citizen] = vote;
                else
                    p.Votes.Add(citizen, vote);
                Polls[poll] = p;
                return Ok();
            }
            return Unauthorized();
        }

        [HttpDelete(Name = "DeletePoll")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult DeletePolls(Guid session, Guid poll)
        {
            if (Sessions.IsValid(session, Privileges.RemovePollArbitrary))
            {
                if (Polls.ContainsKey(poll))
                {
                    Polls.Remove(poll);
                    return Ok();
                }
                else
                    return NotFound();
            }
            else
                return Unauthorized();
        }

        [HttpPut(Name = "CreatePoll")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PutPolls(Guid session, string title, string content, string[] options)
        {
            if (Sessions.IsValid(session, Privileges.AddPoll))
            {
                Guid id;
                do
                    id = Guid.NewGuid();
                while (Polls.ContainsKey(id));
                Polls.Add(id, new Poll() {Content = content, Options = options, Title = title, Votes = new Dictionary<Guid, int>(), Author = Sessions[session].Citizen});
                return id;
            }
            return Unauthorized();
        }

        [HttpPatch(Name = "EditPoll")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PatchPolls(Guid session, Guid poll, string title, string content, string[] options)
        {
            if (Sessions.IsValid(session, Privileges.EditPoll))
            {
                if (Polls.ContainsKey(poll))
                {
                    Poll p = Polls[poll];
                    p.Title = title ?? p.Title;
                    p.Content = content ?? p.Content;
                    p.Options = options ?? p.Options;
                    Polls[poll] = p;
                    return Ok();
                }
                else
                    return NotFound();
            }
            else
                return Unauthorized();
        }
    }
}