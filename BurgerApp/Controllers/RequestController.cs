using System;
using System.Collections.Generic;
using System.Linq;
using BurgerApp.Database;
using Microsoft.AspNetCore.Mvc;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequestController : ControllerBase
    {
        [HttpGet(Name = "GetRequests")]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, Request>))]
        [ProducesResponseType(401)]
        public ActionResult<IDictionary<string, Request>> GetRequests(Guid session)
        {
            if (Sessions.IsValid(session, Privileges.ListRequests))
                return Ok(Requests.GetSendable(session));
            return Unauthorized();
        }
        
        [HttpGet("available", Name = "AreRequestsAvailable")]
        [ProducesResponseType(200, Type = typeof(bool))]
        [ProducesResponseType(401)]
        public ActionResult<bool> GetRequestsAvailable(Guid session)
        {
            if (Sessions.IsValid(session, Privileges.AddRequest))
            {
                DateTime lastRequest = Requests.GetSendable(session, false).Select(s => s.Value.MainMessage.Timestamp)
                    .OrderBy(s => s).LastOrDefault();
                return lastRequest == default || DateTime.Now - lastRequest > new TimeSpan(0, 1, 0);
            }
            return Unauthorized();
        }

        [HttpPut(Name = "CreateRequest")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(403)]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PutRequests(Guid session, string request, string title)
        {
            if (Sessions.IsValid(session, Privileges.AddRequest))
            {
                DateTime lastRequest = Requests.GetSendable(session, false).Select(s => s.Value.MainMessage.Timestamp)
                    .OrderBy(s => s).LastOrDefault();
                if (lastRequest == default || DateTime.Now - lastRequest > new TimeSpan(0, 1, 0))
                {
                    Guid id;
                    do
                        id = Guid.NewGuid();
                    while (Requests.ContainsKey(id));
                    Requests.Add(id, new Request {MainMessage = new Message{Content = request, Author = Sessions[session].Citizen, Timestamp = DateTime.Now}, Title = title});
                    return id;
                }
                else
                    return StatusCode(403, "No two requests may be sent within an hour from another");
            }
            return Unauthorized();
        }

        [HttpDelete(Name = "DeleteRequest")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult DeleteRequests(Guid session, Guid request)
        {
            if (Requests.ContainsKey(request))
            {
                if (Sessions.IsValid(session, Privileges.RemoveRequest, Requests[request].MainMessage.Author)
                    || Sessions.IsValid(session, Privileges.RemoveRequestArbitrary))
                {
                    Requests.Remove(request);
                    return Ok();
                }
                else
                    return Unauthorized();
            }
            else
                return NotFound();
        }

        [HttpPatch("comment", Name = "CreateRequestComment")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public ActionResult PatchRequests(Guid session, Guid request, string message)
        {
            if (Requests.ContainsKey(request))
            {
                if (Sessions.IsValid(session, Privileges.RespondRequest, Requests[request].MainMessage.Author)
                    || Sessions.IsValid(session, Privileges.RespondRequestArbitrary))
                {
                    Request req = Requests[request];
                    Message[] tmp = new[]
                        {new Message {Content = message, Author = Sessions[session].Citizen, Timestamp = DateTime.Now}};
                    req.Answers = req.Answers != null ? req.Answers.Concat(tmp).ToArray() : tmp;
                    Requests[request] = req;
                    return Ok();
                }
                else
                    return Unauthorized();
            }
            else
                return NotFound();
        }
    }
}
