using System;
using Microsoft.AspNetCore.Mvc;
using static BurgerApp.Database.Databases;

namespace BurgerApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SessionController : ControllerBase
    {
        [HttpPut(Name = "OpenSession")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(401)]
        public ActionResult<Guid> PutSession(Auth auth)
        {
            Guid? c = Citizens.GetCitizen(auth);
            if (c == null)
                return Unauthorized();
            Guid id;
            do
                id = Guid.NewGuid();
            while (Requests.ContainsKey(id));
            Sessions.Add(id, new Session {Citizen = c.Value, CreationTime = DateTime.Now});
            return id;
        }

        [HttpGet(Name = "GetUserToken")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(404)]
        public ActionResult<Guid> GetUserToken(Guid session)
        {
            if (!Sessions.ContainsKey(session))
                return NotFound();
            return Sessions[session].Citizen;
        }

        [HttpDelete(Name = "CloseSession")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult DeleteSession(Guid session)
        {
            if (Sessions.Remove(session))
                return Ok();
            return NotFound();
        }

        [HttpPatch(Name = "RefreshSession")]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(404)]
        public ActionResult<Guid> PatchSession(Guid session)
        {
            if (Sessions.ContainsKey(session))
            {
                Guid id;
                do
                    id = Guid.NewGuid();
                while (Requests.ContainsKey(id));
                Sessions.Add(id, new Session {Citizen = GetUserToken(session).Value, CreationTime = DateTime.Now});
                Sessions.Remove(session);
                return Ok(id);
            }
            else
                return NotFound();
        }
    }
}
