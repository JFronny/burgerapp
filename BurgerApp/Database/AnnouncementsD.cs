﻿using CC_Functions.AspNet;

namespace BurgerApp.Database
{
    public class AnnouncementsD : SerialDict<Message>
    {
        public override string DatabasesDir => Databases.Dir;
        public override string DatabaseFileName => $"announcements.{Databases.Extension}";
    }
}