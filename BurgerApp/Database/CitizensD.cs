using System;
using CC_Functions.AspNet;

namespace BurgerApp.Database
{
    public class CitizensD : SerialDict<Citizen>
    {
        public override string DatabasesDir => Databases.Dir;
        public override string DatabaseFileName => $"citizens.{Databases.Extension}";

        private bool _initial = true;

        public CitizensD()
        {
            Loaded += (s) =>
            {
                if (_initial)
                {
                    _initial = false;
                    if (s.Count == 0)
                    {
                        Guid guid = Guid.NewGuid();
                        
                        //This just figures out the highest possible value for privileges whose binary representation consists only of 1s
                        int b = 2;
                        while (b < int.MaxValue / 2) b *= 2;
                        
                        s.Add(guid, new Citizen {Name = new Name {FirstNames = new[] {"Max"}, LastNames = new[] {"Mustermann"}}, PasswordHash = Databases.Hash("1234"), Privileges = (Privileges)(b - 1)});
                    }
                }
            };
        }

        public Guid? GetCitizen(Auth auth)
        {
            string pwdHash = Databases.Hash(auth.Password);
            foreach (Guid key in Keys)
            {
                Citizen citizen = this[key];
                if ($"{citizen.Name.FirstNames[0]}.{citizen.Name.LastNames[0]}" == auth.Username && citizen.PasswordHash == pwdHash)
                    return key;
            }
            return null;
        }
    }
}