using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using BurgerApp.Controllers;

namespace BurgerApp.Database
{
    public static class Databases
    {
        public static readonly string Dir =
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "DB");

        public const string Extension = "db";
        
        public static readonly CitizensD Citizens = new CitizensD();
        public static readonly RequestsD Requests = new RequestsD();
        public static readonly SessionsD Sessions = new SessionsD();
        public static readonly AnnouncementsD Announcements = new AnnouncementsD();
        public static readonly PollsD Polls = new PollsD();

        public static readonly MD5 md5 = MD5.Create();

        public static string Hash(string value)
        {
            byte[] pwdBytes = md5.ComputeHash(Encoding.Unicode.GetBytes(value));
            StringBuilder sb = new StringBuilder();
            foreach (byte t in pwdBytes)
                sb.Append(t.ToString("X2"));
            return sb.ToString();
        }
        
    }
}