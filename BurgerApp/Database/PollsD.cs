using System;
using System.Collections.Generic;
using System.Linq;
using CC_Functions.AspNet;

namespace BurgerApp.Database
{
    public class PollsD : SerialDict<Poll>
    {
        public override string DatabasesDir => Databases.Dir;
        public override string DatabaseFileName => $"polls.{Databases.Extension}";

        public PollsD()
        {
            Loaded += (el) =>
            {
                foreach (Poll kvp in el.Values)
                {
                    if (kvp.Options != null && kvp.Votes != null)
                    {
                        int options = kvp.Options.Length;
                        KeyValuePair<Guid, int>[] arr = kvp.Votes.ToArray();
                        foreach (KeyValuePair<Guid, int> v in arr)
                            if (v.Value >= options)
                                kvp.Votes.Remove(v.Key);
                    }
                    else
                    {
                        kvp.Options ??= new string[0];
                        kvp.Votes = new Dictionary<Guid, int>();
                    }
                }
            };
        }

        public IDictionary<string, AnonymizedPoll> GetAnon()
        {
            return GetSendable().ToDictionary(s => s.Key, s => new AnonymizedPoll
            {
                Content = s.Value.Content,
                Options = s.Value.Options.Select((t, i) => new AnonymizedPoll.Option
                {
                    Title = t,
                    Votes = s.Value.Votes.Values.Count(u => u == i)
                }).ToArray(),
                Title = s.Value.Title,
            });
        }
    }
}