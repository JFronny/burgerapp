using System;
using System.Collections.Generic;
using System.Linq;
using CC_Functions.AspNet;

namespace BurgerApp.Database
{
    public class RequestsD : SerialDict<Request>
    {
        public override string DatabasesDir => Databases.Dir;
        public override string DatabaseFileName => $"requests.{Databases.Extension}";
        
        public IDictionary<string, Request> GetSendable(Guid session, bool includeArbit = true)
        {
            return GetSendable().Where(s =>
            {
                if (includeArbit && Databases.Sessions.IsValid(session, Privileges.ListRequestsArbitrary))
                    return true;
                return s.Value.MainMessage.Author == Databases.Sessions[session].Citizen;
            }).ToDictionary(s => s.Key, s => s.Value);
        }
    }
}