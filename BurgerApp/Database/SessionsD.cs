using System;
using System.Collections.Generic;
using System.Linq;

namespace BurgerApp.Database
{
    public class SessionsD: Dictionary<Guid, Session>
    {
        private void RemoveOld()
        {
            foreach (Guid token in Keys.Select(s => s))
                if (DateTime.Now - this[token].CreationTime > new TimeSpan(0, 30, 0))
                    Remove(token);
        }

        public bool IsValid(Guid session)
        {
            RemoveOld();
            return ContainsKey(session);
        }

        public bool IsValid(Guid session, Privileges requiredPrivs)
        {
            if (!IsValid(session)) return false;
            Guid citizen = this[session].Citizen;
            if (!Databases.Citizens.ContainsKey(citizen)) return false;
            return (Databases.Citizens[citizen].Privileges & requiredPrivs) == requiredPrivs;
        }

        public bool IsValid(Guid session, Privileges requiredPrivs, Guid citizen) => IsValid(session, requiredPrivs) && this[session].Citizen == citizen;
    }
}