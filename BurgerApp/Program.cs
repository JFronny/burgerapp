using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BurgerApp.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using CC_Functions.AspNet;
using CC_Functions.Commandline;

namespace BurgerApp
{
    public class Program
    {
        public static void Main(string[] a)
        {
            ArgsParse args = new(a);
            Environment.CurrentDirectory = Path.GetDirectoryName(Databases.Dir);
            if (args.GetBool("dump"))
            {
                DateTime now = DateTime.Now;
                string dir = Path.Combine(Databases.Dir, $"DUMP_{now}");
                if (Directory.Exists(dir))
                    Directory.Delete(dir, true);
                Directory.CreateDirectory(dir);
                Dump(Databases.Citizens, dir);
                Dump(Databases.Requests, dir);
                Dump(Databases.Announcements, dir);
                Dump(Databases.Polls, dir);
                Console.WriteLine($"Successfully created data dump at {dir}");
                return;
            }
            if (args["import"] != null)
            {
                string dir = args["import"];
                if (!Directory.Exists(dir))
                {
                    Console.WriteLine("Directory not found!");
                    return;
                }
                Import(Databases.Citizens, dir);
                Import(Databases.Requests, dir);
                Import(Databases.Announcements, dir);
                Import(Databases.Polls, dir);
                Console.WriteLine($"Successfully imported data dump from {dir}");
                return;
            }
            CreateHostBuilder(a).Build().Run();
        }

        private static void Import<T>(SerialDict<T> dict, string dir)
        {
            string json = File.ReadAllText(Path.Combine(dir, Path.ChangeExtension(dict.DatabaseFileName, "json")));
            IEnumerable<KeyValuePair<Guid, T>> d = JsonSerializer.Deserialize<IEnumerable<KeyValuePair<Guid, T>>>(json, new JsonSerializerOptions().AddCcf());
            dict.Clear();
            foreach (KeyValuePair<Guid, T> kvp in d) dict.Add(kvp);
        }

        private static void Dump<T>(SerialDict<T> dict, string dir)
        {
            IEnumerable<KeyValuePair<Guid, T>> d = dict.ToArray();
            string json = JsonSerializer.Serialize(d, new JsonSerializerOptions().AddCcf());
            File.WriteAllText(Path.Combine(dir, Path.ChangeExtension(dict.DatabaseFileName, "json")), json);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}