using System.Collections.Generic;
using ProtoBuf;

namespace BurgerApp
{
    public class AnonymizedPoll
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public Option[] Options { get; set; }

        public class Option
        {
            public string Title { get; set; }
            public int Votes { get; set; }
        }
    }
}