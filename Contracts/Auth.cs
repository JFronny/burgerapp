using System;
using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Auth
    {
        [ProtoMember(1)] public string Username { get; set; }
        [ProtoMember(2)] public string Password { get; set; }
    }
}