using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Citizen
    {
        [ProtoMember(1)] public Name Name { get; set; }
        [ProtoMember(2)] public string PasswordHash { get; set; }
        [ProtoMember(3)] public Privileges Privileges { get; set; }
    }
}