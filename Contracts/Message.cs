using System;
using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Message
    {
        [ProtoMember(1)] public Guid Author { get; set; }
        [ProtoMember(2)] public string Content { get; set; }
        [ProtoMember(3)] public DateTime Timestamp { get; set; }
    }

    public class AnonymizedMessage
    {
        public string Content { get; set; }
        public DateTime Timestamp { get; set; }
    }
}