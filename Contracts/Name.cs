using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Name
    {
        [ProtoMember(1)] public string[] FirstNames { get; set; }
        [ProtoMember(2)] public string[] LastNames { get; set; }
    }
}