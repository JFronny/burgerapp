using System;
using System.Collections.Generic;
using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Poll
    {
        [ProtoMember(1)] public string Title { get; set; }
        [ProtoMember(2)] public string Content { get; set; }
        [ProtoMember(3)] public string[] Options { get; set; }
        [ProtoMember(4)] public Dictionary<Guid, int> Votes { get; set; }
        [ProtoMember(5)] public Guid Author { get; set; }
    }
}