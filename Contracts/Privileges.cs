using System;

namespace BurgerApp
{
    [Flags]
    public enum Privileges
    {
        ListCitizens = 1,
        AddCitizen = 2,
        RemoveCitizen = 4,
        
        ListRequests = 8,
        ListRequestsArbitrary = 16,
        AddRequest = 32,
        RemoveRequest = 64,
        RemoveRequestArbitrary = 128,
        RespondRequest = 256,
        RespondRequestArbitrary = 512,
        
        ModifyPassword = 1024,
        ModifyPasswordArbitrary = 2048,
        ModifyPrivilegesArbitrary = 4096,
        ModifyNamesArbitrary = 8192,
        
        ListAnnouncements = 16384,
        AddAnnouncement = 32768,
        DeleteAnnouncement = 65536,
        
        ListPolls = 131072,
        AddPoll = 262144,
        EditPoll = 524288,
        VoteOnPolls = 1048576,
        RemovePollArbitrary = 2097152,
        
        
        
        Default = ListRequests | AddRequest | RemoveRequest | RespondRequest | ModifyPassword | ListAnnouncements | ListPolls | VoteOnPolls
    }
}