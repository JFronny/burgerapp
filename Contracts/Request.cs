using ProtoBuf;

namespace BurgerApp
{
    [ProtoContract]
    public class Request
    {
        [ProtoMember(1)] public string Title { get; set; }
        [ProtoMember(2)] public Message MainMessage { get; set; }
        [ProtoMember(3)] public Message[] Answers { get; set; }
    }

    public class AnonymizedRequest
    {
        public string Title { get; set; }
        public AnonymizedMessage Message { get; set; }
    }
}