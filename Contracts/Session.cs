using System;

namespace BurgerApp
{
    public class Session
    {
        public Guid Citizen { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
