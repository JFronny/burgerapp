FROM mcr.microsoft.com/dotnet/sdk:5.0.102-ca-patch-buster-slim AS build-env
WORKDIR /app

COPY . ./

RUN git clone https://github.com/iconic/open-iconic scss/open-iconic
RUN apt update
RUN apt install -y nodejs npm zip patch
RUN patch -i WebUI/wwwroot/index.diff WebUI/wwwroot/index.html
RUN rm WebUI/wwwroot/index.diff
RUN npm install -g sass
RUN dotnet publish BurgerApp/BurgerApp.csproj -c Release -o out
RUN rm out/BurgerApp

FROM mcr.microsoft.com/dotnet/aspnet:5.0.0-buster-slim-arm32v7
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "BurgerApp.dll"]
