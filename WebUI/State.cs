using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BurgerApp.API;
using Microsoft.AspNetCore.Components;

namespace WebUI
{
    public static class State
    {
        public static bool LoggedIn = false;
        public static Guid? UserToken = null;
        public static Guid? SessionToken = null;
        public static string FullName = "";
#if DEBUG
        public const string ApiUrl = "https://localhost:5001";
#else
        public const string ApiUrl = "https://akbvopenfl1.hopto.org";
#endif
        private static bool _tokenRTaskActive = false;
        private static Citizen _user;
        public static CancellationTokenSource TokenSource = new CancellationTokenSource();

        public static bool HasPerm(BurgerApp.Privileges privs) => UserToken != null && HasPerm(_user, privs);

        public static bool HasPerm(Citizen user, BurgerApp.Privileges privs) => user != null && ((int)user.Privileges & (int)privs) == (int)privs;

        public static async Task<string> GetName(Client client, Guid user)
        {
            try
            {
                return GetName(await GetUser(client, user));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "[unidentified]";
            }
        }

        public static string GetName(Citizen user)
        {
            Name name = user.Name;
            return $"{string.Join(" ", name.FirstNames)} {string.Join(" ", name.LastNames)}";
        }

        public static string GetShort(string from, int maxLengh)
        {
            if (from.Length < maxLengh)
                return from;
            return $"{new string(from.Take(maxLengh - 3).ToArray())}...";
        }

        public static bool ShowRequests = false;
        public static bool ListRequestsArbitrary = false;
        public static bool ListCitizens = false;
        public static bool RemoveCitizen = false;
        public static bool ModifyNamesArbitrary = false;
        public static bool ModifyPassword = false;
        public static bool ModifyPasswordArbitrary = false;
        public static bool ModifyPrivilegesArbitrary = false;
        public static bool ListAnnouncements = false;
        public static bool DeleteAnnouncement = false;
        public static bool AddAnnouncement = false;
        public static bool ListPolls = false;
        public static bool RemovePollArbitrary = false;
        public static bool EditPoll = false;

        public static async Task FetchData(Client Client, NavigationManager Nav)
        {
            try
            {
                _user = await GetUser(Client, UserToken.Value);
                ShowRequests = LoggedIn && (HasPerm(BurgerApp.Privileges.ListRequests) ||
                                            HasPerm(BurgerApp.Privileges.ListRequestsArbitrary));
                ListRequestsArbitrary = HasPerm(BurgerApp.Privileges.ListRequestsArbitrary);
                ListCitizens = HasPerm(BurgerApp.Privileges.ListCitizens);
                RemoveCitizen = HasPerm(BurgerApp.Privileges.RemoveCitizen);
                ModifyNamesArbitrary = HasPerm(BurgerApp.Privileges.ModifyNamesArbitrary);
                ModifyPassword = HasPerm(BurgerApp.Privileges.ModifyPassword);
                ModifyPasswordArbitrary = HasPerm(BurgerApp.Privileges.ModifyPasswordArbitrary);
                ModifyPrivilegesArbitrary = HasPerm(BurgerApp.Privileges.ModifyPrivilegesArbitrary);
                ListAnnouncements = HasPerm(BurgerApp.Privileges.ListAnnouncements);
                DeleteAnnouncement = HasPerm(BurgerApp.Privileges.DeleteAnnouncement);
                AddAnnouncement = HasPerm(BurgerApp.Privileges.AddAnnouncement);
                ListPolls = HasPerm(BurgerApp.Privileges.ListPolls);
                RemovePollArbitrary = HasPerm(BurgerApp.Privileges.RemovePollArbitrary);
                EditPoll = HasPerm(BurgerApp.Privileges.EditPoll);
                Nav.NavigateTo(Nav.Uri);
#pragma warning disable 4014
                TokenRefreshTask(new CancellationTokenSource(), Client);
#pragma warning restore 4014
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static async Task TokenRefreshTask(CancellationTokenSource tokenSource, Client Client)
        {
            if (!_tokenRTaskActive)
            {
                _tokenRTaskActive = true;
                TokenSource = tokenSource;
                while (true)
                {
                    try
                    {
                        await Task.Delay(new TimeSpan(0, 5, 0));
                        if (tokenSource.IsCancellationRequested)
                        {
                            _tokenRTaskActive = false;
                            return;
                        }
                        SessionToken = await Client.RefreshSessionAsync(SessionToken);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        public static async Task<Citizen> GetUser(Client Client, Guid user) => (await Client.GetCitizensAsync(SessionToken))
            .First(s => s.Key == user.ToString()).Value;
    }
}