# EDIT: swagger instance is included, this is thereby obsolete

These examples assume the endpoint to be "https://localhost:5001", you should change that for public services

## getCitizens
list all citizens

```
curl --location --request GET 'https://localhost:5001/api/citizen?token=035961a1-030d-4b71-8622-1ebf17573236'
```

## putCitizens
add a citizen, returns its ID

```
curl --location --request PUT 'https://localhost:5001/api/citizen?token=035961a1-030d-4b71-8622-1ebf17573236' \
--header 'Content-Type: application/json' \
--data-raw '{
    "firstNames": [
        "John"
    ],
    "names": [
        "Smith"
    ]
}'
```

## deleteCitizen (remove a citizen by its id):

```
curl --location --request DELETE 'https://localhost:5001/api/citizen?token=035961a1-030d-4b71-8622-1ebf17573236&citizen=62d06080-81e7-4cd8-9620-6e90c7ec16f8'
```

## getTokens
list all tokens. Note that this includes their Guid. This can be used to gain all other privileges, use with caution

```
curl --location --request GET 'https://localhost:5001/api/token?token=035961a1-030d-4b71-8622-1ebf17573236'
```

## putTokens
add a token, change the integer to modify privileges, see Contracts/Privileges.cs, returns the Guid

```
curl --location --request PUT 'https://localhost:5001/api/token?token=035961a1-030d-4b71-8622-1ebf17573236' \
--header 'Content-Type: application/json' \
--data-raw '{
    "privileges": 16383
}'
```

## deleteTokens
removes a token by its guid. The token to remove is "val", the token to check for privileges is "token"

```
curl --location --request DELETE 'https://localhost:5001/api/token?token=035961a1-030d-4b71-8622-1ebf17573236&val=b70eba0c-4e88-4278-ab10-243a6df0ff0f'
```
