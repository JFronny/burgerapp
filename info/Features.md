Api:

- [x] Authentication
- [X] Session
- [X] Ratelimited requests (text + author guid, ability to have answers), visible to the mayor
- [X] Announcements (Pieces of text, ordered by date. Similar in structure to requests but only sendable by admins, without reactions and visible to all)
- [X] Polls ("Do you like this idea", "What is the better implementation", text, bool: result visible to users, answer array (strings), reaction array (guids & index))

Security:

- [X] Authentication for Citizens: Username "FirstFirstName.FirstLastName" + Password
- [X] "Admin" password

Management web UI:

- [X] Reset passwords for citizens
- [X] Add/remove/edit citizens
- [X] Add/remove announcements
- [X] Add/remove/view polls

User web UI:

- [X] Sign in/out
- [X] send/remove requests
- [X] Comments on requests
- [X] settings view (password, full name etc)
- [X] view announcements
- [X] view/vote on polls

Android App:

TODO fill this in
TODO this might use something link .NET MAUI (will take longer, ~Nov 2021, but has the advantage of being the same language as everything else here) or the java APIs (Java is not that hard tbh), I have yet to decide that

Known problems:

TODO there might be scenarios where insufficient privileges cause errors, this should be tested

Additional considerations:

TODO I should consider editing functionality for existing data types (PATCH operations)
