Required options:
- Header text
- Main text

/\ These describe the reason for the poll. While this could be done through announcements that would be unneccessarily cubersome
- Votes:
  - Multiple options described by text. If the number of these is changed related votes need to be updated

Operations:
- Close/Reopen poll (disable voting)
- Remove poll
- Edit poll WITHOUT loosing votes. If the data structure is a dictionary with user IDs as keys and vote indexes as values this should be possible
- Create new poll: choose texts, whether it is enabled

User operations:
- Vote
- Change vote
- Change vote
