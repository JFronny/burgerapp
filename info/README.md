You might need to change the following things before using this code:
- `<base href="/burgerapp/" />` in index.html only works if the WebUI is hosted in that directory. You can replace the href with "/" for a normal install.
- `public const string ApiUrl = "http://akbvopenfl1.hopto.org:5000/";` in State.cs (also WebUI) should be the URL of your backend install. This one is just for testing.
